; #############################################################################
; ARBK_P1_KnightRiderLight.asm
;
; ARBK - FH Aachen - WS2017/2018
;
; Created: 21.10.2017
; Author : Fabian Sch�ttler
; #############################################################################
; Notes:
; - StateMachine with 2 states for shifting active led
;   - state_0 : shift led left
;   - state_1 : shift led right
; - implemented with delay loops
;
; #############################################################################
.INCLUDE <m8def.inc>   ; includes controller labels
.CSEG                   ; start code segment

; CPU Frequency (Hz)
.equ f_cpu = 3686000

; define some labels for working registers
.def temp = r16
.def leds = r17
.def state = r18

; set shift limits
.equ led_mask = 0xE0
.equ led_high = 7
.equ led_low =  5

; Interrupt Vector Table
.org 0x0000
    rjmp    init        ; Reset vector

; reserve progmem for full vectortable
.org INT_VECTORS_SIZE

; begin of main routine
init:
    ; init stack
    ldi     temp, HIGH(RAMEND)
    out     SPH, temp
    ldi     temp, LOW(RAMEND)
    out     SPL, temp

    ; init PortD
    ; set PD7, PD6, PD5 as output
    ldi     temp, led_mask
    out     DDRD, temp
    ; set PD7, PD6, PD5 as high (inverted logic for leds)
    ldi     leds, 0
    ;ldi     leds, led_mask
    out     PORTD, leds
    ; init state machine
    ldi     leds, (1 << led_low)    ; start with left shifts on PD5
    ldi     state, 0                ; start with state_0 (left shifts)

; entry for main loop
loop:
    ; wait
    rcall   delay_100ms
    rcall   delay_100ms

    ; state machine
state_0:    ; left shift
    cpi     state, 0
    ; if state != 0, continue with state_1
    brne    state_1
    ; else
    lsl     leds                    ; leftshift active led
    cpi     leds, (1 << led_high)   ; check leds for left limit
    brne    state_end               ; goto end, if not reached limit
    ldi     state, 1                ; switch state, if reached limit
    rjmp    state_end

state_1:    ; shift right
    cpi     state, 1
    ; if state != 1 , branch state_0
    brne    state_0
    ; else
    lsr     leds                    ; rightshift active led
    cpi     leds, (1 << led_low)    ; check leds for right limit
    brne    state_end               ; goto end, if not reached limit
    ldi     state, 0                ; switch state, if reached limit
    rjmp    state_end

state_end:  ; output
    in      temp, PORTD             ; read PORTD
    ANDI    temp, (!led_mask)       ; clear masked bits
    OR      temp, leds              ; set active led
    ;ldi     r19, led_mask           ; load r19 with led_mask
    ;EOR     temp, r19               ; invert bits masked by led_mask
    out     PORTD, temp             ; set PORTD
    rjmp    loop

    ; never reached
    rjmp    -1

; #############################################################################

/*
 * Waitfunctions
 * clocks for rcall and return included in calculation
 */
; wait for exactly 1ms
delay_1ms:
.equ d1ms = (((f_cpu/1000) - 24) / 8)
    ldi     r25, HIGH(d1ms)     ; load wordregister
    ldi     r24, LOW(d1ms)
    rjmp    delay

; wait for exactly 10ms
delay_10ms:
.equ d10ms = (((f_cpu/100) - 24) / 8)
    ldi     r25, HIGH(d10ms)    ; load wordregister
    ldi     r24, LOW(d10ms)
    rjmp    delay

; wait for exactly 100ms
delay_100ms:
.equ d100ms = (((f_cpu/10) - 24) / 8)
    ldi     r25, HIGH(d100ms)   ; load wordregister
    ldi     r24, LOW(d100ms)
    rjmp    delay

; wait loop
delay:
    sbiw    r24, 1              ; dec wordregister
    nop                         ; some nops to double looptime
    nop
    nop
    nop
    brne    delay               ; leave loop on zero
    nop                         ; more nops to fill missing clocks
    nop
    nop
    nop
    nop
    nop
    ret
